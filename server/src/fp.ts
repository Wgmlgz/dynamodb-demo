import { faker } from '@faker-js/faker';

export interface FP {
  clientId: string;
  cameraId: string;
  featureId: string;
  cycleId: string;
  ageArr: number[]; // i32
  valueArr: number[]; // f32
  timestampArr: number[]; // i32
  junkArr: boolean[];
}

export const randomFP = (vec_length: number, camera_id?: string): FP => ({
  clientId: 'testCLient',
  cameraId: faker.datatype.string(),
  featureId: 'testFeature',
  cycleId: 'testCycle',
  ageArr: Array(vec_length).fill(1),
  valueArr: Array(vec_length).fill(0.1),
  timestampArr: Array(vec_length).fill(1),
  junkArr: Array(vec_length).fill(true),
});
