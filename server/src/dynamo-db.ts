import AWS from 'aws-sdk';
import dynamo from 'dynamodb';
import Joi from 'joi';
import { FP, randomFP } from './fp';
AWS.config.update({
  region: 'us-east-1', // replace with your region
  apiVersion: '2012-08-10',
});
dynamo.AWS.config.update({
  region: 'us-east-1', // replace with your region
  apiVersion: '2012-08-10',
});

export const FPModel = dynamo.define<FP>('test_graph', {
  hashKey: 'cameraId',
  schema: {
    // email: Joi.string().email(),
    // name: Joi.string(),
    // age: Joi.number(),
    // roles: dynamo.types.stringSet(),
    // settings: {
    //   nickname: Joi.string(),
    //   acceptedTerms: Joi.boolean().default(false),
    // },

    clientId: Joi.string(),
    cameraId: Joi.string(),
    featureId: Joi.string(),
    cycleId: Joi.string(),
    ageArr: Joi.array().items(Joi.number()),
    valueArr: Joi.array().items(Joi.number()),
    timestampArr: Joi.array().items(Joi.number()),
    junkArr: Joi.array().items(Joi.boolean()),
  },
});
dynamo.createTables(function (err) {
  if (err) {
    console.error('Error creating tables: ', err);
  } else {
    // console.log('Tables has been created');
  }
});

export const connect = async () => {
  // AWS.config.update({
  //   region: 'us-east-1', // replace with your region
  //   apiVersion: '2012-08-10',
  // });
  dynamo.AWS.config.update({
    region: 'us-east-1', // replace with your region
    apiVersion: '2012-08-10',
  });
  // const dynamodb = new AWS.DynamoDB({
  //   region: 'us-east-1', // replace with your region
  //   apiVersion: '2012-08-10',
  // });

  // return dynamodb;
};

export const createTestRandom = async (n: number, vec_length: number) => {
  await Promise.all(
    Array.from({ length: n }).map(async () => {
      const t = await FPModel.create(randomFP(vec_length));
    })
  );
  console.log('huh?');
  // await FPModel.create(randomFP(10))
  // console.log('huh?')
  // await FPModel.create(
  //   Array.from({ length: n }, () => randomFP(vec_length, 'test'))
  // );
};

export const readTest = async () => {
  const t = (await FPModel.scan().loadAll().exec().promise())
    .map(x => x.Items)
    .flat();
  console.log(t.length);
};

export const init = async () => {};
export const clearTest = async () => {
  await FPModel.destroy('test');
};
