import * as DynamoFP from './dynamo-db';
import * as DocumentFP from './document-db';
import * as dotenv from 'dotenv';
dotenv.config();

/**
puts 10000 items in database 
 */

const testFP = async (fp: typeof DynamoFP | typeof DocumentFP) => {
  await fp.connect();
  console.log('connect done');
  await fp.clearTest();
  console.log('clear done');
  await fp.createTestRandom(100, 1000);
  console.log('create done');
  const startTime = performance.now();
  await fp.readTest();
  const endTime = performance.now();
  console.log(`read took ${endTime - startTime} milliseconds`);
  console.log('read done');
};

test('DynamoDB FP 10000 insert + read', async () => {
  await testFP(DynamoFP);
}, 600000);

// test('DocumentDB FP 10000 insert + read', async () => {
//   await testFP(DocumentFP);
// }, 600000);
