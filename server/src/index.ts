import express from 'express';
import cors from 'cors';
import dynamo from 'dynamodb';
import AWS from 'aws-sdk';
import * as FP from './dynamo-db';
import mongoose from 'mongoose';
import { FPModel } from './document-db';
const app = express();
app.use(cors());
require('dotenv').config();

// main().catch(err => console.log(err));

// async function main() {
//   console.log('1');
//   await mongoose.connect(
//     'mongodb://TestDocDB:TestDocDB@docdb-2023-03-16-10-11-02.cluster-cjgjxkqdkbj1.us-east-1.docdb.amazonaws.com:27017',

//     'mongodb://TestDocDB:<insertYourPassword>@docdb-2023-03-16-10-11-02.cluster-cjgjxkqdkbj1.us-east-1.docdb.amazonaws.com:27017'
//     {
//       ssl: true,
//       sslValidate: true,
//       sslCA: `rds-combined-ca-bundle.pem`,
//     }
//   );
//   console.log('2');

//   const t = await FPModel.findOne({});
//   console.log(t);
//   console.log('3');
// }

const dynamodb = new AWS.DynamoDB({
  region: 'us-east-1', // replace with your region
  apiVersion: '2012-08-10',
});

app.get('/', (req, res) => {
  res.send('Hello, world1!');
});

const sortByKey = <T>(array: T[], key: keyof T) =>
  array.sort((a, b) => {
    const x = a[key];
    const y = b[key];
    return x < y ? -1 : x > y ? 1 : 0;
  });

app.get('/data', async (req, res) => {
  const params = {
    TableName: 'test_graph', // replace with your table name
  };
  await FP.createTestRandom(10, 10)
  dynamodb.scan(params, (err, data) => {
    if (err) {
      console.error(err);
      res.status(500).json({ message: 'Internal server error' });
    } else {
      const chartData = sortByKey(
        data.Items.map(item => ({
          x: Number(item.x.N),
          y: Number(item.y.N),
        })),
        'x'
      );
      res.json(chartData);
    }
  });
  // res.json(data);
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

setInterval(async () => {
  const params = {
    TableName: 'test_graph', // replace with your table name
    Item: {
      id: { S: 'test_graph' },
      timestamp: { N: Date.now().toString() },
      x: { N: Math.floor(Math.random() * 100).toString() },
      y: { N: Math.floor(Math.random() * 100).toString() },
    },
  };
  const res = await dynamodb.putItem(params); 
}, 50000);
