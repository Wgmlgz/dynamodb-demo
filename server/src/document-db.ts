import mongoose from 'mongoose';
import { randomFP } from './fp';

const uri = process.env.MONGO_URL;

export const connect = async () => {
  await mongoose.connect(uri, { tlsCAFile: `rds-combined-ca-bundle.pem` });
  console.log('Connected to DocumentDB');
};

const Schema = mongoose.Schema;

const FPSchema = new Schema({
  clientId: String,
  cameraId: String,
  featureId: String,
  cycleId: String,
  ageArr: [Number],
  valueArr: [Number],
  timestampArr: [Number],
  junkArr: [Number],
});
export const FPModel = mongoose.model('FP', FPSchema);

export const createTestRandom = async (n: number, vec_length: number) => {
  // await Promise.all(
  //   Array(n).map(async () => {
  //     await FPModel.create(randomFP(vec_length));
  //   })
  // );
  const t = await FPModel.insertMany(Array(n).map(() => randomFP(vec_length)));
  console.log(t);
};

export const readTest = async () => {
  const t = await FPModel.find({});
  console.log(t.length);
};

export const clearTest = async () => {
  await FPModel.deleteMany({ cameraId: 'test' });
};

export const readFP = async () => {};
