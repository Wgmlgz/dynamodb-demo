source setup.sh
aws lambda delete-event-source-mapping --uuid $(aws lambda list-event-source-mappings | jq -r ".EventSourceMappings[] | select(.EventSourceArn == \"${KINESIS_STREAM}\") | .UUID")
aws kinesis delete-stream --stream-name $STREAM_NAME