console.log('Loading function');
const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

exports.handler = async (event, context) => {
  //console.log('Received event:', JSON.stringify(event, null, 2));
  await Promise.all(
    event.Records.map(
      record =>
        new Promise((resolve, reject) => {
          // Kinesis data is base64 encoded so decode here
          const payload = Buffer.from(record.kinesis.data, 'base64').toString(
            'ascii'
          );
          console.log('Decoded payload:', payload);

          const n = Number(payload);
          console.log('Number:', n);

          const r = n * 10 + 5;
          console.log('Result:', r);

          const tableName = 'test_graph';
          dynamodb.putItem(
            {
              TableName: tableName,
              Item: {
                id: { S: 'test_graph' },
                timestamp: { N: Date.now().toString() },
                x: { N: r.toString() },
                y: { N: Math.floor(Math.random() * 100).toString() },
              },
            },
            (err, data) => {
              if (err) {
                console.log('Error putting item into dynamodb failed: ' + err);
                reject();
              } else {
                console.log(
                  'great success: ' + JSON.stringify(data, null, '  ')
                );
                resolve();
              }
            }
          );
        })
    )
  );
  return `Successfully processed ${event.Records.length} records.`;
};
