source setup.sh
aws kinesis create-stream --stream-name $STREAM_NAME
aws lambda create-event-source-mapping --batch-size 100 --starting-position LATEST --enabled --function-name $FUNCTION_NAME  --event-source-arn $KINESIS_STREAM