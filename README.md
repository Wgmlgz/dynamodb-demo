# Демо проект с графиком и dynamodb

## Установка и запуск

сервер раз в 50с добавляет рандомную точку в таблицу test_graph

Фронт отображает график, можно обновить по кнопке.

### Данные для авторизации

<https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/loading-node-credentials-shared.html>

Нужно записать в `C:\Users\USER_NAME\.aws\credentials` эти строчки:

```toml
[default]
aws_access_key_id = <YOUR_ACCESS_KEY_ID>
aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>
```

Как получить `aws_access_key_id` и `aws_secret_access_key`:

- Войдите в свою консоль управления AWS.
- Перейдите к сервису "IAM" (Управление идентификацией и доступом).
- Нажмите на "Пользователи" в левой панели навигации.
- Выберите пользователя, для которого вы хотите сгенерировать ключи доступа.
- Перейдите на вкладку "Учетные данные для обеспечения безопасности".
- Прокрутите вниз до раздела "Ключи доступа" и нажмите "Создать ключ доступа".
- Там можно сгенерить данные для аутентификации и вставить их в `C:\Users\USER_NAME\.aws\credentials`

### Запуск

после того, как записали данные для авторизации, можно запускать проект

Эта команда одновременно запустит клиент и сервер

```sh
npm i
npm run build
npm run start
```

Окно с проектом (<http://127.0.0.1:5173/>) должно открыться само.

## Dynamodb

В консоли управления (<https://us-east-1.console.aws.amazon.com/dynamodbv2/home?region=us-east-1#tables>) добавил таблицу test_graph c ключем id (строка) и ключем для сортировки timestamp (число).
Dynamodb использует 2 ключа `Partition key` и `Sort key` (опциональный), по этим ключам можно искать и сорить доки.

### Работа с dynamodb в js

подключить dynamodb к проекту

```js
const AWS = require('aws-sdk');

const dynamodb = new AWS.DynamoDB({
  region: 'us-east-1',
  apiVersion: '2012-08-10',
});
```

получить данные из таблицы:

```js
const params = {
  TableName: 'test_graph',
};
dynamodb.scan(params, (err, data) => {
  if (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  } else {
    const chartData = sortByKey(
      data.Items.map(item => ({
        x: Number(item.x.N),
        y: Number(item.y.N),
      })),
      'x'
    );
    res.json(chartData);
  }
});
```

добавить документ в таблицу:

```js
const params = {
  TableName: 'test_graph',
  Item: {
    id: { S: 'test_graph' },
    timestamp: { N: Date.now().toString() },
    x: { N: Math.floor(Math.random() * 100).toString() },
    y: { N: Math.floor(Math.random() * 100).toString() },
  },
};
dynamodb.putItem(params, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});
```

# Kinesis

<https://docs.aws.amazon.com/streams/latest/dev/tutorial-stock-data-kplkcl2-create-stream.html>

`arn:aws:kinesis:us-east-1:780743977125:stream/DemoStream`

- Засунуть данные в поток

  ```sh
  aws kinesis put-record --stream-name DemoStream --data 666 --partition-key 6666 --cli-binary-format raw-in-base64-out
  ```

- Прочитать данные из потока

  - Берем итератор `aws kinesis get-shard-iterator` возвращает `json`

  ```sh
  aws kinesis get-shard-iterator \
      --stream-name DemoStream \
      --shard-id shardId-000000000003 \
      --shard-iterator-type TRIM_HORIZON
  ```

  - По значению итератора получаем данные

  ```sh
  aws kinesis get-records \
      --shard-iterator $(aws kinesis get-shard-iterator     --stream-name DemoStream     --shard-id shardId-000000000003     --shard-iterator-type TRIM_HORIZON | jq '.ShardIterator')
  ```
